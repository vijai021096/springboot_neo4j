package com.example.demo.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	public Collection<User> getAll(){
		return userRepository.getAllUsers();
	}
	
	public void postData(User user) {
		userRepository.save(user);
	}
}
