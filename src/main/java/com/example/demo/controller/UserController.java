package com.example.demo.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/api/demo/user")
public class UserController {

	
	@Autowired
	UserService userService;
	
	@GetMapping("/allUsers")
	public Collection<User>getAllUsers(){
		 return userService.getAll();
	}
	
	@PostMapping("/saveUser")
	public void saveUsers(@RequestBody User user) {
		
		 userService.postData(user);
	}
}
